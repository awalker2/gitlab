export default () => ({
  filterValue: {
    id: null,
    type: null,
  },

  startDate: null,
  endDate: null,

  sortBy: null,
});
